package tfs.naval

object Solution {

  // определить, подходит ли корабль по своим характеристикам
  def validateShip(ship: Ship): Boolean = true

  // определить, можно ли его поставить
  def validatePosition(ship: Ship, field: Field): Boolean = true

  // добавить корабль во флот
  def enrichFleet(fleet: Fleet, name: String, ship: Ship): Fleet = ???

  // добавить корабль на карту
  def markUsedCells(field: Field, ship: Ship): Field = ???

  //
  def tryAddShip(game: Game, name: String, ship: Ship): Game = ???
}