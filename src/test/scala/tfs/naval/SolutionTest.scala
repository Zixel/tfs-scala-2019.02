package tfs.naval

import org.scalatest.{FlatSpec, Matchers}

class SolutionTest extends FlatSpec with Matchers{

  "ships" should "be linear" in {

    val ship = Lesson.input(0)._2

    Solution.validateShip(ship) shouldBe true

    val ship2 = List(
      (1,1),
      (1,2),
      (2,2)
    )

    Solution.validateShip(ship2) shouldBe true


    val ship3 = List()

    Solution.validateShip(ship3) shouldBe true



  }

  "ships" should "be no longer then 4" in {

    val ship = Lesson.input(0)._2

    Solution.validateShip(ship) shouldBe true

    val ship2 = Lesson.input(1)._2

    Solution.validateShip(ship2) shouldBe true

    val ship3 = List(
      (1,1),
      (1,2),
      (1,3),
      (1,4),
      (1,5)
    )

    Solution.validateShip(ship3) shouldBe false
  }

  "empty ship" should " be invalid while checking lenght" in {
    val ship = List()
    Solution.validateShip(ship) shouldBe false
  }

  "ships" should "be placed separately" in {

    val field = Vector(
      Vector(false,false,false,false,false),
      Vector(false,true,false,false,false),
      Vector(false,true,false,false,false),
      Vector(false,true,false,false,false),
      Vector(false,false,false,false,false)
    )

    //Валидный корабль
    val ship = List(
      (1,1),
      (1,2)
    )

    Solution.validatePosition(ship, field) shouldBe true

    //Невалидный корабль
    val ship2 = List(
      (2,2),
      (2,3),
      (2,4)
    )

    Solution.validatePosition(ship2, field) shouldBe false


    //Валидный корабль из урока (там поле из одних false)
    val ship3 = Lesson.input(0)._2
    Solution.validatePosition(ship3, Lesson.field) shouldBe true
    //Валидный корабль из урока (там поле из одних false)
    val ship4 = Lesson.input(1)._2
    Solution.validatePosition(ship4, Lesson.field) shouldBe true

  }

  "empty ship" should " be invalid while placing" in {
    //"Пустой" корабль
    val ship5 = List()
    Solution.validatePosition(ship5, Lesson.field) shouldBe false
  }


}
